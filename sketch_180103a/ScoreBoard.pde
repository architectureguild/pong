public class ScoreBoard implements Observer, IDrawable {
  private int _p1Score, _p2Score;
  private PFont _f;

  public ScoreBoard()
  {
    _p1Score = 0;
    _p2Score = 0;
    _f = createFont("Arial", 32, true);
    textFont(_f);
    textAlign(CENTER);
  }

  @Override
    public void Update(String state) {
    if (state.endsWith("P1")) { //<>//
      _p1Score++;
    } else if (state.endsWith("P2")) {
      _p2Score++;
    }
  }

  public void Draw()
  {
    text(_p1Score + " | " + _p2Score, width/2, 60);
  }
}